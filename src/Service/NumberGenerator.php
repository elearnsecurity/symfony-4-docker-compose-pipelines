<?php

namespace App\Service;

use Exception;

/**
 * NumberGenerator.
 */
class NumberGenerator
{
    /**
     * Generates a random integer number within input range
     *
     * @param int $min The range lower bound
     * @param int $max The range upper bound
     *
     * @return int
     *
     * @throws Exception
     */
    public function generateRandomNumberWithinRange(int $min, int $max)
    {
        return random_int(0, 100);
    }
}