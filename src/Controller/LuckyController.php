<?php

namespace App\Controller;

use App\Service\NumberGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * LuckyController.
 */
class LuckyController extends AbstractController
{
    /**
     * @Route("/lucky/number")
     *
     * @param NumberGenerator $numberGenerator The number generator service
     *
     * @return Response
     */
    public function number(NumberGenerator $numberGenerator)
    {
        $number = $numberGenerator->generateRandomNumberWithinRange(1, 100);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
}