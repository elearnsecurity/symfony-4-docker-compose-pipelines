#!/usr/bin/env bash

# https://github.com/ralish/bash-script-template/blob/stable/template.sh
set -o errexit  # Exit on most errors (see the manual)
set -o pipefail # Use last non-zero exit code in a pipeline

# DESC: Initializes the script.
function script_init() {
  readonly INITIALIZED_FILE_PATH='/.initialized'
  readonly PHP_REQUIRED_EXTENSIONS_FILE_PATH='/docker-entrypoint-init.d/php_extensions'
  readonly PHP_CONFIGURATION_PATH='/docker-entrypoint-init.d/php'
  readonly PHP_EXTENSIONS_CONFIGURATION_PATH='/docker-entrypoint-init.d/php/php.d'
  readonly PHPFPM_CONFIGURATION_PATH='/docker-entrypoint-init.d/php-fpm'
  readonly PHPFPM_POOL_CONFIGURATION_PATH='/docker-entrypoint-init.d/php-fpm/pools'
  readonly SCRIPTS_PATH='/docker-entrypoint-init.d/scripts'
}

# DESC: Install additional PHP extensions.
install_additional_php_extensions() {
  echo "Install additional PHP extensions..."

  yum update -y

  if [[ ! -e "${PHP_REQUIRED_EXTENSIONS_FILE_PATH}" ]]; then
    echo "You have not defined a file containing additional PHP extensions at path ${PHP_REQUIRED_EXTENSIONS_FILE_PATH}."
    return 0
  fi

  # https://stackoverflow.com/questions/10929453/read-a-file-line-by-line-assigning-the-value-to-a-variable
  while IFS= read -r php_extension; do
    echo "Install ${php_extension}"
    yum install "${php_extension}" -y
  done < ${PHP_REQUIRED_EXTENSIONS_FILE_PATH}
}

# DESC: Creates both user and group using provided ennvironment variables.
create_user_and_group() {
  echo "Create both user and group..."

  if [[ $(id -u app > /dev/null 2>&1; echo $?) == "0" ]]; then
    echo "User 'app' already exists."
    return 0
  fi

  if [[ -z "${HOST_USER_ID}" || -z "${HOST_GROUP_ID}" ]]; then
    echo "You have to define both HOST_USER_ID and HOST_GROUP_ID environment variables."
    exit 1
  fi

  groupadd -g "${HOST_GROUP_ID}" app && useradd -u "${HOST_USER_ID}" -g app -s /bin/bash app
}

# DESC: Copies PHP configuration.
copy_php_configuration() {
  # PHP
  if [[ -d "${PHP_CONFIGURATION_PATH}" ]]; then
    if [[ -e "${PHP_CONFIGURATION_PATH}/php.ini" ]]; then
      echo "Copy ${PHP_CONFIGURATION_PATH}/php.ini to /etc/php.ini"
      \cp ${PHP_CONFIGURATION_PATH}/php.ini /etc/php.ini
    fi
    # PHP extensions configuration folder
    if [[ -d "${PHP_EXTENSIONS_CONFIGURATION_PATH}" ]]; then
      for php_extension_configuration in "${PHP_EXTENSIONS_CONFIGURATION_PATH}"/*.ini; do
        [ -e "$php_extension_configuration" ] || continue
        echo "Copy ${php_extension_configuration} in /etc/php.d folder."
        \cp "$php_extension_configuration" /etc/php.d/
      done
    else
      echo "You have not defined a directory containing additional PHP extensions configuration at path ${PHP_EXTENSIONS_CONFIGURATION_PATH}."
    fi
  else
    echo "You have not defined a directory containing additional PHP configuration at path ${PHP_CONFIGURATION_PATH}."
  fi

  # PHP-FPM main folder
  if [[ -d "${PHPFPM_CONFIGURATION_PATH}" ]]; then
    if [[ -e "${PHPFPM_CONFIGURATION_PATH}/php-fpm.conf" ]]; then
      echo "Copy ${PHPFPM_CONFIGURATION_PATH}/php-fpm.conf to /etc/php-fpm.conf"
      \cp ${PHPFPM_CONFIGURATION_PATH}/php-fpm.conf /etc/php-fpm.conf
    fi
    # PHP-FPM pools folder
    if [[ -d "${PHPFPM_POOL_CONFIGURATION_PATH}" ]]; then
      for phpfpm_pool_configuration in "${PHPFPM_POOL_CONFIGURATION_PATH}"/*.conf; do
        [ -e "$phpfpm_pool_configuration" ] || continue
        echo "Copy ${phpfpm_pool_configuration} in /etc/php-fpm.d folder."
        \cp "$phpfpm_pool_configuration" /etc/php-fpm.d/
      done
    else
      echo "You have not defined a directory containing additional PHP-FPM pools configuration at path ${PHPFPM_POOL_CONFIGURATION_PATH}."
    fi
  else
    echo "You have not defined a directory containing additional PHP-FPM configuration at path ${PHPFPM_CONFIGURATION_PATH}."
  fi
}

# DESC: Executes additional scripts.
execute_scripts() {
  if [[ -d "${SCRIPTS_PATH}" ]]; then
    for script in "${SCRIPTS_PATH}"/*.sh; do
      if [[ -x "${script}" ]]; then
        echo "Running ${script}."
        . "${script}"
      else
        echo "Script ${script} is not executable. It will be skipped."
      fi
    done
  else
    echo "You have not defined a directory containing additional scripts at path ${SCRIPTS_PATH}."
  fi
}

# DESC: Initializes the container.
initialize() {
  create_user_and_group
  install_additional_php_extensions
  copy_php_configuration
  execute_scripts

  touch ${INITIALIZED_FILE_PATH}
}

# DESC: Checks whether the container is initialized or not. If not, initializes it. After that run php-fpm.
main() {
  script_init

  if [[ ! -e "${INITIALIZED_FILE_PATH}" ]]; then
    echo "Initialize PHP-FPM-BASE container..."
    initialize
  else
    echo "PHP-FPM-BASE container is already initialized."
  fi

  echo "Start php-fpm..."
  php-fpm
}

main "$@"