<?php

namespace App\Tests\Service;

use App\Service\NumberGenerator;
use PHPUnit\Framework\TestCase;

/**
 * NumberGeneratorTest.
 *
 * @coversDefaultClass \App\Service\NumberGenerator
 */
class NumberGeneratorTest extends TestCase
{
    /**
     * Tests that the function correctly generate a number within specified range.
     *
     * @covers ::generateRandomNumberWithinRange
     */
    public function testGenerateRandomNumberWithinRange()
    {
        $numberGenerator = new NumberGenerator();

        $number = $numberGenerator->generateRandomNumberWithinRange(1, 100);

        $this->assertIsInt($number);
        $this->assertGreaterThan(1, $number);
        $this->assertLessThan(100, $number);
    }
}