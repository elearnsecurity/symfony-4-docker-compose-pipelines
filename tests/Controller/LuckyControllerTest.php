<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * LuckyControllerTest.
 *
 * @coversDefaultClass \App\Controller\LuckyController
 */
class LuckyControllerTest extends WebTestCase
{
    /**
     * Tests that a page with a random number is shown.
     *
     * @covers ::number
     */
    public function testNumber()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/lucky/number');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(1, $crawler->filter('h1'));
        $this->assertCount(1, $crawler->filter('h3'));
        $this->assertIsInt((int)$crawler->filter('#lucky-number-h3')->text(null, true));

    }
}