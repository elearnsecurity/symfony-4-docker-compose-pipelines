# Symfony 4 - Docker Compose

Symfony 4.4 application with development environment powered by Docker Compose.

## Provisioning

To ease local development you have to install these tools:

* [Docker CE](https://www.docker.com/) - At least version 18.06.0
* [Docker-Compose](https://docs.docker.com/compose/)

**Note**: It is recommended to follow [this guide](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user) in order to manage Docker as a non-root user.

In the project's root folder there is the main file where the architecture is described: `docker-compose.yml`. 
In there you can find all services configured that you'll get. 
In order to have everything work correctly you have to copy the `.env.dist` file to the `.env`. 
The latter holds all the environment variables used by docker-compose to run all the containers.

To bring up all the containers, from project's root folder, run:

```
docker-compose up --build
```

**Note**: If you want to run in detached mode just add the `--detached` option to the `up` command.

You can monitor all active containers running:

```
docker-compose ps
```

At this point you have to connect to *php-fpm* container and download all required dependencies needed for the project executing this command:

```
docker-compose exec -u app php-fpm bash -c 'composer install'
```

To stop all the containers just run:

```
docker-compose stop
```

To destroy the environment just run:

```
docker-compose down
```

**Note**: If you want to destroy also the volumes add the `--volumes` option to the `down` command.


In order to work with Traefik, the user has two available options. 
The first one is to add following lines to the machine's host file:

```
127.0.0.1 www.symfony-4-docker-dev.com
127.0.0.1 phpmyadmin.symfony-4-docker-dev.com
```

The second one is to properly configure the DNS.

## What you get
In the provided architecture you have following containers with all the described tools within them:

* php-fpm
    * CentOS 7.6
    * PHP-FPM 7.1
    * Composer 1.8
    * NodeJS 10
    * Yarn 1.13
* www
    * CentOS 7.6
    * Apache 2.4
* mysql
    * MySQL 5.7
* phpmyadmin
    * phpMyAdmin Last available version
* traefik
    * Traefik 2.1

## Asynchronous tasks

### Access MySQL with phpMyAdmin

The development environment comes with phpMyAdmin already installed.
You can reach the management UI by browsing
to [this page](http://phpmyadmin.symfony-4-docker-dev.com) and logging in as root user
using the credential specified in the key *MYSQL_ROOT_PASSWORD* stored in the `.env` file.